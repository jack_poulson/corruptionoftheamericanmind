import matplotlib.pyplot as plt
import statsmodels.formula.api as sm
import pandas as pd

def fit(df,
        x_key,
        y_key='TotalAttacks',
        x_label=None,
        y_label='Total Attacks',
        title=None,
        figure=None):
    print('Testing regressions of {} ~ {}'.format(y_key, x_key))
    result = sm.ols(formula="{} ~ {}".format(y_key, x_key), data=df).fit()
    r_squared = result.rsquared
    intercept = result.params['Intercept']
    coefficient = result.params[x_key]
    print(result.params)
    print(result.summary())
    plt.figure(figure) # if figure is None, a new figure is created
    plt.scatter(df[x_key], df[y_key])
    plt.plot(df[x_key], intercept + coefficient * df[x_key], '-.')
    if x_label:
        plt.xlabel(x_label)
    if y_label:
        plt.ylabel(y_label)
    r_squared_subtitle = 'R^2: {0:.3g}'.format(r_squared)
    if title:
        title = '{}\n{}'.format(title, r_squared_subtitle)
    else:
        title = r_squared_subtitle
    plt.title(title)


def fit_with_cutoff(df,
                    x_key,
                    sort_key,
                    cutoff,
                    y_key='TotalAttacks',
                    x_label=None,
                    y_label='Total Attacks',
                    title=None,
                    figure=None):
    if cutoff != None:
        df_top = df.sort_values(sort_key, ascending=False)[:cutoff]
        if title != None:
            title += ' (Top {})'.format(cutoff)
    else:
        df_top = df
    fit(df_top,
        x_key,
        y_key=y_key,
        x_label=x_label,
        y_label=y_label,
        title=title,
        figure=figure)


def fit_cutoff_sequence(df,
                        x_key,
                        sort_key,
                        cutoffs,
                        y_key='TotalAttacks',
                        x_label=None,
                        y_label='Total Attacks',
                        title=None):
    for cutoff in cutoffs:
        print('Testing with x_key={}, sort_key={}, cutoff={}'.format(x_key, sort_key, cutoff))
        fit_with_cutoff(df,
                        x_key,
                        sort_key,
                        cutoff,
                        y_key=y_key,
                        x_label=x_label,
                        y_label=y_label,
                        title=title)


# Analyze the original data set.
print('Analyzing the original (N=293) dataset.')
df = pd.read_csv('ScholarlyAttacks.csv', sep='|')
# Recalibrate undisclosed donations in terms of billions of dollars.
df['ForeignFunding'] = df['ForeignFunding'] / 1.e9
sort_key = 'ForeignFunding'
cutoffs = [None, 25]
fit_cutoff_sequence(
    df,
    'ForeignFunding',
    sort_key,
    cutoffs,
    x_label='Foreign Funding (Billion USD)',
    title='FIRE Scholars Under Fire attacks vs. Foreign Funding')
plt.show()

# Analyze the condensed dataset extended to include university endowment sizes.
print('Analyzing the condensed (N=260) dataset joined with endowment sizes.')
df = pd.read_csv('ScholarlyAttacksWithAssets.csv', sep='|')
# Recalibrate foreign funding in terms of billions of dollars.
df['ForeignFunding'] = df['ForeignFunding'] / 1.e9
sort_key = 'ForeignFunding'
cutoffs = [None, 25]
fit_cutoff_sequence(
    df,
    'ForeignFunding',
    sort_key,
    cutoffs,
    x_label='Foreign Funding (Billion USD)',
    title='FIRE Scholars Under Fire attacks vs. Foreign Funding')
fit_cutoff_sequence(df,
                    'AssetsB',
                    sort_key,
                    cutoffs,
                    x_label='Endowment (Billion USD)',
                    title='FIRE Scholars Under Fire attacks vs. Endowment')
plt.show()
